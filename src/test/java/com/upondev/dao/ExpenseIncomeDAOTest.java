package com.upondev.dao;

import com.upondev.model.Income;
import com.upondev.services.DataHelper;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class ExpenseIncomeDAOTest {

    @Test
    public void ExpenseIncomeDAO_StringNameIntUserIdEntityClass_ListOfTypeSize1() {
        // given
        String userName = "TestUser";
        String typeName = "Husband incomes";
        Date date = DataHelper.stringToDateForView("2020-02-25");
        String entryName = "Salary";
        double entryValue = 2000.0;
        Class entityClass = Income.class;

        // when
        int quantityBefore = ExpenseIncomeDAO.getEntries(userName, entityClass).size();
        ExpenseIncomeDAO.createEntry(userName, typeName, date, entryName, entryValue, entityClass);

        //then
        int quantityAfter = ExpenseIncomeDAO.getEntries(userName, entityClass).size();

        // assert
        int result = quantityAfter - quantityBefore;
        assertEquals(1, result);


    }


}
