package com.upondev.controller;

import com.upondev.dao.UsersDAO;
import com.upondev.model.*;
import com.upondev.services.Check;
import com.upondev.services.DAOHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;


public class UserOptionsController {
    private String incorrectPasswordText =
            "Incorrect password:" +
                    "\n  -minimum eight characters, " +
                    "\n  -at least one uppercase letter, " +
                    "\n  -one lowercase letter, " +
                    "\n  -one number, " +
                    "\n  -one special character.";

    @FXML
    private TextField resetUserNameTextField;
    @FXML
    private TextField resetEmailTextField;
    @FXML
    private PasswordField resetPasswordNewPasswordTextField;
    @FXML
    private TextField deleteUserNameTextField;
    @FXML
    private PasswordField deletePasswordTextField;
    @FXML
    private Button resetPasswordButton;
    @FXML
    private Button deleteUserButton;
    @FXML
    private Label userOptionsLabel;

    @FXML
    private void resetPasswordAction(ActionEvent event) {
        if (Check.findUserForResetPassword(
                resetUserNameTextField.getText(),
                resetEmailTextField.getText())) {
            if (Check.passwordRegex(resetPasswordNewPasswordTextField.getText())) {
                UsersDAO usersDAO = new UsersDAO();
                Users oldUser = usersDAO.getUser(resetUserNameTextField.getText());
                Users updateUser = new Users();

                updateUser.setUserId(oldUser.getUserId());
                updateUser.setUserName(oldUser.getUserName());
                updateUser.setEmail(oldUser.getEmail());
                updateUser.setEntryDate(oldUser.getEntryDate());
                updateUser.setPass(resetPasswordNewPasswordTextField.getText());

                DAOHelper.hibernateSessionMerge(updateUser);
                userOptionsLabel.setText("Password changed successfully!");
            } else {
                userOptionsLabel.setText(incorrectPasswordText);
            }
        } else {
            userOptionsLabel.setText("Incorrect user or email");
        }
    }

    @FXML
    private void deleteUserButtonAction(ActionEvent event) {
        if (Check.findUserForLogin(
                deleteUserNameTextField.getText(),
                deletePasswordTextField.getText())) {
            UsersDAO.deleteAllUserEntriesFromOneTable(deleteUserNameTextField.getText(), UserExpenseType.class);
            UsersDAO.deleteAllUserEntriesFromOneTable(deleteUserNameTextField.getText(), UserIncomeType.class);
            UsersDAO.deleteAllUserEntriesFromOneTable(deleteUserNameTextField.getText(), Expense.class);
            UsersDAO.deleteAllUserEntriesFromOneTable(deleteUserNameTextField.getText(), Income.class);
            UsersDAO.deleteAllUserEntriesFromOneTable(deleteUserNameTextField.getText(), Users.class);
            userOptionsLabel.setText("User and all date deleted successfully!");

        } else {
            userOptionsLabel.setText("Incorrect user or email");
        }
    }

}
