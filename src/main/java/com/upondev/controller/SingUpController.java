package com.upondev.controller;

import com.upondev.dao.UsersDAO;
import com.upondev.services.Check;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.event.ActionEvent;


public class SingUpController {
    @FXML
    private Label statusLabel;

    @FXML
    private TextField userNameTextField;

    @FXML
    private TextField emailTextField;

    @FXML
    private PasswordField passwordTextField;

    @FXML
    private Button createUserButton;

    private String incorrectPasswordText =
            "Incorrect user name or password. " +
                    "\nPassword:" +
                    "\n  -minimum eight characters, " +
                    "\n  -at least one uppercase letter, " +
                    "\n  -one lowercase letter, " +
                    "\n  -one number, " +
                    "\n  -one special character.";

    // @FXML
    // public void passwordTextFieldOnMouse() {
    //     Tooltip tooltip = new Tooltip();
    //     tooltip.setText("Your password must be " +
    //             "\n - Minimum eight characters length, " +
    //             "\n - at least one uppercase letter," +
    //             "\n - one lowercase letter," +
    //             "\n - one number and one special character");
    //
    //     passwordTextField.setTooltip(tooltip);
    // }

    @FXML
    public void createNewUserButtonAction(ActionEvent event) {

        if (!Check.checkOrUserNameExist(userNameTextField.getText())) {
            if (Check.emailRegex(emailTextField.getText())) {
                if (Check.passwordRegex(passwordTextField.getText())) {
                    UsersDAO user = new UsersDAO();
                    user.createUser(userNameTextField.getText(), emailTextField.getText(), passwordTextField.getText());
                    statusLabel.setText("User " + userNameTextField.getText() + " crated!");
                } else {
                    statusLabel.setText(incorrectPasswordText);
                }
            } else {
                statusLabel.setText(incorrectPasswordText);
            }

        } else {
            statusLabel.setText("User " + userNameTextField.getText() + " already exist");
        }

    }

}
