package com.upondev.controller;

import com.upondev.dao.DefaultTypesDAO;
import com.upondev.dao.ExpenseIncomeDAO;
import com.upondev.dao.UserCreatedTypesDAO;
import com.upondev.model.*;
import com.upondev.services.DataHelper;
import com.upondev.services.NumberHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.event.ActionEvent;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.time.LocalDate;
import java.util.*;

public class UserDataController implements Initializable {
    // Expenses tab
    // --------------Create Expense
    @FXML
    private Label expErrorMessageLabel;
    @FXML
    private DatePicker expCreateDatePicker;
    @FXML
    private ChoiceBox expTypeChoiceBox;
    @FXML
    private TextField expNameText;
    @FXML
    private TextField expValueText;
    @FXML
    private Button createButton;

    // ------------- Load Expenses
    @FXML
    private DatePicker ExpenseDatePickerFrom;
    @FXML
    private DatePicker expenseDatePickerTo;
    @FXML
    private ChoiceBox expenseForLoadTypeChoiceBox;
    @FXML
    private Button loadExpensesButton;

    // ------------- Expense TableView
    @FXML
    private TableView<Expense> expTableView;
    @FXML
    private TableColumn<Expense, Date> expDateColumn;
    @FXML
    private TableColumn<Expense, Integer> expTypeColumn;
    @FXML
    private TableColumn<Expense, String> expNameColumn;
    @FXML
    private TableColumn<Expense, Double> expValueColumn;
    @FXML
    private TableColumn<Expense, Button> expUpdateColumn;
    @FXML
    private TableColumn<Expense, Button> expDeleteColumn;

    // Incomes tab
    // --------------Create Income
    @FXML
    private Label incErrorMessageLabel;
    @FXML
    private DatePicker incCreateDatePicker;
    @FXML
    private ChoiceBox incTypeChoiceBox;
    @FXML
    private TextField incNameText;
    @FXML
    private TextField incValueText;
    @FXML
    private Button incCreateButton;

    // ------------- Load Incomes
    @FXML
    private DatePicker incomeDatePickerFrom;
    @FXML
    private DatePicker incomeDatePickerTo;
    @FXML
    private ChoiceBox incomeForLoadTypeChoiceBox;
    @FXML
    private Button loadIncomeButton;

    // ------------- Incomes TableView
    @FXML
    private TableView<Income> incTableView;
    @FXML
    private TableColumn<Income, Date> incDateColumn;
    @FXML
    private TableColumn<Income, Integer> incTypeColumn;
    @FXML
    private TableColumn<Income, String> incNameColumn;
    @FXML
    private TableColumn<Income, Double> incValueColumn;
    @FXML
    private TableColumn<Income, Button> incUpdateColumn;
    @FXML
    private TableColumn<Income, Button> incDeleteColumn;

    // Types tab
    // Expenses
    @FXML
    private Label typesExpenseErrorMessageLabel;
    @FXML
    private TextField createExpenseNameTextField;
    @FXML
    private Button createExpenseTypeButton;
    @FXML
    private TableView<UserExpenseType> expenseTypesTableView;
    @FXML
    private TableColumn<String, UserExpenseType> expenseTypesColumn;
    @FXML
    private TableColumn<Button, UserExpenseType> expenseTypeEditColumn;
    @FXML
    private TableColumn<Button, UserExpenseType> expenseTypeDeleteColumn;

    // Incomes
    @FXML
    private Label typesIncomeErrorMessageLabel;
    @FXML
    private TextField createIncomeNameTextField;
    @FXML
    private Button createIncomeTypeButton;
    @FXML
    private TableView<UserIncomeType> incomesTypesTableView;
    @FXML
    private TableColumn<String, UserIncomeType> incomeTypesColumn;
    @FXML
    private TableColumn<Button, UserIncomeType> incomesTypeEditButton;
    @FXML
    private TableColumn<Button, UserIncomeType> incomeTypesDeleteButton;

    // Expenses pie chart table
    @FXML
    private PieChart expensesPieChart;
    @FXML
    private Label expensesPieChartStatisticLabel;
    @FXML
    private DatePicker expensesPieChartDateFrom;
    @FXML
    private DatePicker expensesPieChartDateTo;
    @FXML
    private Button expensesPieChartLoadDataButton;
    @FXML
    private Button expensesAllDataButton;

    // Incomes pie chart table
    @FXML
    private PieChart incomesPieChart;
    @FXML
    private Label incomesPieChartStatisticLabel;
    @FXML
    private DatePicker incomesPieChartDateFrom;
    @FXML
    private DatePicker incomesPieChartDateTo;
    @FXML
    private Button PieChartLoadDataButton;
    @FXML
    private Button incomesAllDataButton;

    // Balance tab
    @FXML
    private DatePicker balanceAreaChartDateFrom;
    @FXML
    private DatePicker balanceAreaChartDateTo;
    @FXML
    private Button balanceChartLoadDataButton;
    // @FXML
    // private Button balanceAllDataButton;
    @FXML
    private AreaChart balanceAreaChart;

    @FXML
    private void expensesAllDataButtonAction() {
        expensesPieChartLoad();
    }

    @FXML
    private void IncomesAllDataButtonAction() {
        incomesPieChartLoad();
    }

    @FXML
    private void expensesPieChartLoadDataButtonAction(ActionEvent event) {
        expensesPieChartLoadBetweenTwoDays();
    }

    @FXML
    private void incomesPieChartLoadDataButtonAction(ActionEvent event) {
        incomesPieChartLoadBetweenTwoDays();
    }

    @FXML
    public void expenseCreateButtonAction(ActionEvent event) {
        expErrorMessageLabel.setText("");
        createExpenseEntry();
        expNameText.clear();
        expValueText.clear();
        expTableView.getItems().clear();
        loadExpensesCurrentMothDataToExpensesTableView();
    }

    @FXML
    public void incomesCreateButtonAction(ActionEvent event) {
        incErrorMessageLabel.setText("");
        createIncomeEntry();
        incNameText.clear();
        incValueText.clear();
        incTableView.getItems().clear();
        loadIncomesCurrentMothDataToIncomesTableView();
    }

    @FXML
    public void createExpenseTypeButtonAction(ActionEvent event) {
        typesExpenseErrorMessageLabel.setText("");
        createExpenseTypeEntry();
        createExpenseNameTextField.clear();
        expenseTypesTableView.getItems().clear();
        userCreatedExpensesTypesLoadToTable();
    }

    @FXML
    public void createIncomeTypeButtonAction(ActionEvent event) {
        typesIncomeErrorMessageLabel.setText("");
        createIncomeTypeEntry();
        createIncomeNameTextField.clear();
        incomesTypesTableView.getItems().clear();
        userCreatedIncomeTypesLoadToTable();

    }

    @FXML
    public void loadExpensesButtonAction(ActionEvent event) {
        expTableView.getItems().clear();
        Date dateF = DataHelper.localDateToDate(ExpenseDatePickerFrom);
        Date dateT = DataHelper.localDateToDate(expenseDatePickerTo);
        expenseTableColumnSetCellValue();

        if (expenseForLoadTypeChoiceBox.getValue().equals(" Type Selection")) {
            expensesBetweenTwoDaysLoadToTable(dateF, dateT);
        } else {
            expensesBetweenTwoDaysAndTypeLoadToTable(dateF, dateT);
        }
    }

    @FXML
    public void loadIncomesButtonAction(ActionEvent event) {
        incTableView.getItems().clear();
        Date dateF = DataHelper.localDateToDate(incomeDatePickerFrom);
        Date dateT = DataHelper.localDateToDate(incomeDatePickerTo);
        incomesTableColumnSetCellValue();
        if (incomeForLoadTypeChoiceBox.getValue().equals(" Type Selection")) {
            incomesBetweenTwoDaysLoadToTable(dateF, dateT);
        } else {
            incomesBetweenTwoDaysAndTypeLoadToTable(dateF, dateT);
        }

    }

    @FXML
    public void setBalanceAllDataButtonAction(ActionEvent event) {
        balanceAreaChart.getData().clear();
        loadBalanceAreaChart(
                DataHelper.localDateToDate(balanceAreaChartDateFrom.getValue()),
                DataHelper.localDateToDate(balanceAreaChartDateTo.getValue())
        );
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pressEnterKey();
        loadExpensesCurrentMothDataToExpensesTableView();
        loadIncomesCurrentMothDataToIncomesTableView();

        loadDateToFields();
        datePickerOptions();
        choiceBoxOptions();
        expenseTypesList();
        incomeTypesList();
        expensesPieChartLoad();
        incomesPieChartLoad();
        loadBalanceAreaChart(
                DataHelper.localDateToDate(DataHelper.tomorrowDay().minusYears(1)),
                DataHelper.localDateToDate(DataHelper.tomorrowDay())
        );

        //Editable TableView cell
        //https://www.youtube.com/watch?v=z4LVoLg6ch0
        //TODO
        expTableView.setEditable(true);
        // expNameColumn.setCellValueFactory(TextFieldTableCell.forTableColumn());
        // expValueColumn.setCellValueFactory(TextFieldTableCell.forTableColumn());

    }

    //TODO
    public void changeExpenseNameCellEvent(TableColumn.CellEditEvent editedCell) {
        Expense expenseSelected = expTableView.getSelectionModel().getSelectedItem();
        expenseSelected.setEntryName(editedCell.getNewValue().toString());
    }

    public void changeExpenseValueCellEvent(TableColumn.CellEditEvent editedCell) {
        Expense expenseSelected = expTableView.getSelectionModel().getSelectedItem();
        expenseSelected.setEntryValue(Double.parseDouble((String) editedCell.getNewValue()));
    }

    private void loadBalanceAreaChart(Date dateFrom, Date dateTo) {
        XYChart.Series XYExpenses = new XYChart.Series();
        XYExpenses.setName("Expenses");
        XYChart.Series XYIncomes = new XYChart.Series();
        XYIncomes.setName("Incomes");

        balanceAreaChartExpensesLine(dateFrom, dateTo, XYExpenses);
        balanceAreaChartIncomesLine(dateFrom, dateTo, XYIncomes);
        balanceAreaChart.getData().addAll(XYExpenses, XYIncomes);
    }

    private void balanceAreaChartIncomesLine(Date dateFrom, Date dateTo, XYChart.Series XYIncomes) {
        List<Object> incomes = ExpenseIncomeDAO.getEntriesBetweenTwoDays(
                LoginController.getLoginUserName(),
                dateFrom,
                dateTo,
                Income.class);
        Map<String, Double> incomesSumByMonth = new TreeMap<>();

        for (int i = 0; i < incomes.size(); i++) {
            Income income = (Income) incomes.get(i);
            String month = income.getEntryDate().toString().substring(0, 7);
            double value = income.getEntryValue();

            if (incomesSumByMonth.containsKey(month)) {
                double tempValue = incomesSumByMonth.get(month);
                tempValue += value;
                incomesSumByMonth.put(month, tempValue);
            } else {
                incomesSumByMonth.put(month, value);
            }
        }

        for (Map.Entry<String, Double> entry : incomesSumByMonth.entrySet()) {
            XYIncomes.getData().addAll(new XYChart.Data(entry.getKey(), entry.getValue()));

        }
    }

    private void balanceAreaChartExpensesLine(Date dateFrom, Date dateTo, XYChart.Series XYExpenses) {
        List<Object> expenses = ExpenseIncomeDAO.getEntriesBetweenTwoDays(
                LoginController.getLoginUserName(),
                dateFrom,
                dateTo,
                Expense.class);
        Map<String, Double> expensesSumByMonth = new TreeMap<>();

        for (int i = 0; i < expenses.size(); i++) {
            Expense expense = (Expense) expenses.get(i);
            String month = expense.getEntryDate().toString().substring(0, 7);
            double value = expense.getEntryValue();

            if (expensesSumByMonth.containsKey(month)) {
                double tempValue = expensesSumByMonth.get(month);
                tempValue += value;
                expensesSumByMonth.put(month, tempValue);
            } else {
                expensesSumByMonth.put(month, value);
            }
        }

        for (Map.Entry<String, Double> entry : expensesSumByMonth.entrySet()) {
            XYExpenses.getData().addAll(new XYChart.Data(entry.getKey(), entry.getValue()));

        }
    }

    private void incomesPieChartLoadBetweenTwoDays() {
        Map<String, Double> expensesDateBetweenTwoDays = ExpenseIncomeDAO.typesValuesBetweenToDaysSum(
                LoginController.getLoginUserName(),
                DataHelper.localDateToDate(incomesPieChartDateFrom.getValue()),
                DataHelper.localDateToDate(incomesPieChartDateTo.getValue()),
                Income.class
        );

        ObservableList<PieChart.Data> incomesPieChartData = FXCollections.observableArrayList();
        for (Map.Entry<String, Double> entry : expensesDateBetweenTwoDays.entrySet()) {
            incomesPieChartData.addAll(new PieChart.Data(
                    entry.getKey(),
                    NumberHelper.roundTwoDem(entry.getValue())
            ));
        }

        incomesPieChart.setData(incomesPieChartData);
        incomesPieChart.setLegendSide(Side.LEFT);
        incomesPieChart.getData().stream()
                .forEach(data -> {
                    data.getNode().addEventHandler(MouseEvent.ANY, e -> {
                        incomesPieChartStatisticLabel.setText(data.getName() + " " + data.getPieValue() + "€");
                    });
                });
    }

    private void expensesPieChartLoadBetweenTwoDays() {
        Map<String, Double> expensesDateBetweenTwoDays = ExpenseIncomeDAO.typesValuesBetweenToDaysSum(
                LoginController.getLoginUserName(),
                DataHelper.localDateToDate(expensesPieChartDateFrom.getValue()),
                DataHelper.localDateToDate(expensesPieChartDateTo.getValue()),
                Expense.class
        );


        ObservableList<PieChart.Data> expensesPieChartData = FXCollections.observableArrayList();
        for (Map.Entry<String, Double> entry : expensesDateBetweenTwoDays.entrySet()) {
            expensesPieChartData.addAll(new PieChart.Data(
                    entry.getKey(),
                    NumberHelper.roundTwoDem(entry.getValue())
            ));
        }

        expensesPieChart.setData(expensesPieChartData);
        expensesPieChart.setLegendSide(Side.LEFT);
        expensesPieChart.getData().stream()
                .forEach(data -> {
                    data.getNode().addEventHandler(MouseEvent.ANY, e -> {
                        expensesPieChartStatisticLabel.setText(data.getName() + " " + data.getPieValue() + "€");
                    });
                });
    }

    private void expensesPieChartLoad() {
        Map<String, Double> allExpenses = ExpenseIncomeDAO.typesValuesSum(LoginController.getLoginUserName(), Expense.class);
        ObservableList<PieChart.Data> expensesPieChartData = FXCollections.observableArrayList();
        for (Map.Entry<String, Double> entry : allExpenses.entrySet()) {
            expensesPieChartData.addAll(new PieChart.Data(
                    entry.getKey(),
                    NumberHelper.roundTwoDem(entry.getValue())
            ));

        }
        expensesPieChart.setData(expensesPieChartData);
        expensesPieChart.setLegendSide(Side.LEFT);

        expensesPieChart.getData().stream()
                .forEach(data -> {
                    data.getNode().addEventHandler(MouseEvent.ANY, e -> {
                        expensesPieChartStatisticLabel.setText(data.getName() + " " + data.getPieValue() + "€");
                    });
                });
    }

    private void incomesPieChartLoad() {
        Map<String, Double> allIncomes = ExpenseIncomeDAO.typesValuesSum(LoginController.getLoginUserName(), Income.class);
        ObservableList<PieChart.Data> incomesPieChartData = FXCollections.observableArrayList();
        for (Map.Entry<String, Double> entry : allIncomes.entrySet()) {
            incomesPieChartData.addAll(new PieChart.Data(
                    entry.getKey(),
                    NumberHelper.roundTwoDem(entry.getValue())
            ));

        }
        incomesPieChart.setData(incomesPieChartData);
        incomesPieChart.setLegendSide(Side.LEFT);

        incomesPieChart.getData().stream()
                .forEach(data -> {
                    data.getNode().addEventHandler(MouseEvent.ANY, e -> {
                        incomesPieChartStatisticLabel.setText(data.getName() + " " + data.getPieValue() + "€");
                    });
                });
    }

    private void createExpenseTypeEntry() {
        if (createExpenseNameTextField.getText().equalsIgnoreCase("")) {
            typesExpenseErrorMessageLabel.setText("Please write expense type name");
        } else {
            UserCreatedTypesDAO.createType(
                    LoginController.getLoginUserName(),
                    createExpenseNameTextField.getText(),
                    UserExpenseType.class
            );
        }
    }

    private void createIncomeTypeEntry() {
        if (createIncomeNameTextField.getText().equalsIgnoreCase("")) {
            typesIncomeErrorMessageLabel.setText("Please write income type name");
        } else {
            UserCreatedTypesDAO.createType(
                    LoginController.getLoginUserName(),
                    createIncomeNameTextField.getText(),
                    UserIncomeType.class
            );
        }
    }

    private void createExpenseEntry() {
        if (expTypeChoiceBox.getValue().equals(" Type Selection") || expValueText.getText().equalsIgnoreCase("")) {
            expErrorMessageLabel.setText("Please choose the type of expense and write price");
        } else {
            ExpenseIncomeDAO.createEntry(
                    LoginController.getLoginUserName(),
                    expTypeChoiceBox.getValue().toString(),
                    DataHelper.localDateToDate(expCreateDatePicker),
                    expNameText.getText(),
                    Double.parseDouble(expValueText.getText()),
                    Expense.class);
        }

    }

    private void createIncomeEntry() {
        if (incTypeChoiceBox.getValue().equals(" Type Selection") || incValueText.getText().equalsIgnoreCase("")) {
            incErrorMessageLabel.setText("Please choose the type of income and write profit");
        } else {
            ExpenseIncomeDAO.createEntry(
                    LoginController.getLoginUserName(),
                    incTypeChoiceBox.getValue().toString(),
                    DataHelper.localDateToDate(incCreateDatePicker),
                    incNameText.getText(),
                    Double.parseDouble(incValueText.getText()),
                    Income.class
            );
        }
    }

    private void expenseTableColumnSetCellValue() {
        expDateColumn.setCellValueFactory(new PropertyValueFactory<>("entryDate")); //entryDate entity column name
        expTypeColumn.setCellValueFactory(new PropertyValueFactory<>("typeName"));
        expNameColumn.setCellValueFactory(new PropertyValueFactory<>("entryName"));
        expValueColumn.setCellValueFactory(new PropertyValueFactory<>("entryValue"));
    }

    public void incomesTableColumnSetCellValue() {
        incDateColumn.setCellValueFactory(new PropertyValueFactory<>("entryDate")); //entryDate entity column name
        incTypeColumn.setCellValueFactory(new PropertyValueFactory<>("typeName"));
        incNameColumn.setCellValueFactory(new PropertyValueFactory<>("entryName"));
        incValueColumn.setCellValueFactory(new PropertyValueFactory<>("entryValue"));

    }

    public void typesExpensesTableColumnCellValue() {
        expenseTypesColumn.setCellValueFactory(new PropertyValueFactory<>("typeName"));
    }

    public void typesIncomesTableColumnCellValue() {
        incomeTypesColumn.setCellValueFactory(new PropertyValueFactory<>("typeName"));
    }

    private void expensesBetweenTwoDaysLoadToTable(Date dateF, Date dateT) {
        List<Object> expenses;
        expenses = ExpenseIncomeDAO.getEntriesBetweenTwoDays(
                LoginController.getLoginUserName(),
                dateF,
                dateT,
                Expense.class
        );

        for (int i = 0; i < expenses.size(); i++) {
            Expense expense = new Expense();
            expense = (Expense) expenses.get(i);
            expTableView.getItems().add(expense);
        }
    }

    private void incomesBetweenTwoDaysLoadToTable(Date dateF, Date dateT) {
        List<Object> incomes = ExpenseIncomeDAO.getEntriesBetweenTwoDays(
                LoginController.getLoginUserName(),
                dateF,
                dateT,
                Income.class
        );

        for (int i = 0; i < incomes.size(); i++) {
            Income income = new Income();
            income = (Income) incomes.get(i);
            incTableView.getItems().add(income);
        }
    }

    private void userCreatedExpensesTypesLoadToTable() {
        List<Object> userCreatedTypes = UserCreatedTypesDAO.getUserCreatedTypes(
                LoginController.getLoginUserName(),
                UserExpenseType.class
        );

        List<UserExpenseType> types = new LinkedList<>();
        for (int i = 0; i < userCreatedTypes.size(); i++) {
            UserExpenseType type = (UserExpenseType) userCreatedTypes.get(i);
            types.add(type);
        }

        Collections.sort(types, Comparator.comparing(UserExpenseType::getTypeName));

        for (int i = 0; i < types.size(); i++) {
            UserExpenseType userExpenseType = types.get(i);
            expenseTypesTableView.getItems().add(userExpenseType);
        }
    }

    private void userCreatedIncomeTypesLoadToTable() {
        List<Object> userCreatedTypes = UserCreatedTypesDAO.getUserCreatedTypes(
                LoginController.getLoginUserName(),
                UserIncomeType.class
        );

        List<UserIncomeType> types = new LinkedList<>();
        for (int i = 0; i < userCreatedTypes.size(); i++) {
            UserIncomeType type = (UserIncomeType) userCreatedTypes.get(i);
            types.add(type);
        }

        Collections.sort(types, Comparator.comparing(UserIncomeType::getTypeName));

        for (int i = 0; i < types.size(); i++) {
            UserIncomeType userIncomeType = types.get(i);
            incomesTypesTableView.getItems().add(userIncomeType);
        }
    }

    private void expensesBetweenTwoDaysAndTypeLoadToTable(Date dateF, Date dateT) {
        List<Object> expenses;
        expenses = ExpenseIncomeDAO.entriesBetweenTwoDaysAndType(
                LoginController.getLoginUserName(),
                dateF,
                dateT,
                expenseForLoadTypeChoiceBox.getValue().toString(),
                Expense.class
        );
        for (int i = 0; i < expenses.size(); i++) {
            Expense expense = new Expense();
            expense = (Expense) expenses.get(i);
            expTableView.getItems().add(expense);
        }
    }

    private void incomesBetweenTwoDaysAndTypeLoadToTable(Date dateF, Date dateT) {
        List<Object> incomes;
        incomes = ExpenseIncomeDAO.entriesBetweenTwoDaysAndType(
                LoginController.getLoginUserName(),
                dateF,
                dateT,
                incomeForLoadTypeChoiceBox.getValue().toString(),
                Income.class
        );
        for (int i = 0; i < incomes.size(); i++) {
            Income income = new Income();
            income = (Income) incomes.get(i);
            incTableView.getItems().add(income);
        }
    }

    private void loadDateToFields() {
        // Load user created expense types
        typesExpensesTableColumnCellValue();
        userCreatedExpensesTypesLoadToTable();

        //Load user created income types
        typesIncomesTableColumnCellValue();
        userCreatedIncomeTypesLoadToTable();
    }

    private void loadIncomesCurrentMothDataToIncomesTableView() {
        incomesTableColumnSetCellValue();
        incomesBetweenTwoDaysLoadToTable(
                DataHelper.localDateToDate(DataHelper.firstMonthDay()),
                DataHelper.localDateToDate(DataHelper.tomorrowDay())
        );
    }

    private void loadExpensesCurrentMothDataToExpensesTableView() {
        expenseTableColumnSetCellValue();
        expensesBetweenTwoDaysLoadToTable(
                DataHelper.localDateToDate(DataHelper.firstMonthDay()),
                DataHelper.localDateToDate(DataHelper.tomorrowDay())
        );
    }

    private void pressEnterKey() {
        // Create expense entry, then press enter in expenses value text field.
        expValueText.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                createExpenseEntry();
            }
        });

        // Create income entry, then press enter in income value text field.
        expValueText.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                createIncomeEntry();
            }
        });

        // Create expense type entry, then press enter in expense type name text field
        createExpenseNameTextField.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                createExpenseTypeEntry();
            }
        });

        // Create income type entry, then press enter in income type name text field
        createExpenseNameTextField.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                createIncomeTypeEntry();
            }
        });

    }

    private void expenseTypesList() {
        List<Object> defaultTypes = DefaultTypesDAO.getDefaultTypes(DefaultExpensesType.class);
        List<Object> userCreatedTypes = UserCreatedTypesDAO.getUserCreatedTypes(LoginController.getLoginUserName(), UserExpenseType.class);
        List<String> types = new LinkedList<>();
        for (int i = 0; i < defaultTypes.size(); i++) {
            DefaultExpensesType defaultExpensesType = (DefaultExpensesType) defaultTypes.get(i);
            types.add(defaultExpensesType.getTypeName());
        }
        for (int i = 0; i < userCreatedTypes.size(); i++) {
            UserExpenseType userExpenseType = (UserExpenseType) userCreatedTypes.get(i);
            types.add(userExpenseType.getTypeName());
        }
        Collections.sort(types);
        for (int i = 0; i < types.size(); i++) {
            this.expTypeChoiceBox.getItems().add(types.get(i));
            this.expenseForLoadTypeChoiceBox.getItems().add(types.get(i));
        }
    }

    private void incomeTypesList() {
        List<Object> defaultTypes = DefaultTypesDAO.getDefaultTypes(DefaultIncomeType.class);
        List<Object> userCreatedTypes = UserCreatedTypesDAO.getUserCreatedTypes(LoginController.getLoginUserName(), UserIncomeType.class);
        List<String> types = new LinkedList<>();
        for (int i = 0; i < defaultTypes.size(); i++) {
            DefaultIncomeType defaultIncomeType = (DefaultIncomeType) defaultTypes.get(i);
            types.add(defaultIncomeType.getTypeName());
        }
        for (int i = 0; i < userCreatedTypes.size(); i++) {
            UserIncomeType userIncomeType = (UserIncomeType) userCreatedTypes.get(i);
            types.add(userIncomeType.getTypeName());
        }
        Collections.sort(types);
        for (int i = 0; i < types.size(); i++) {
            this.incTypeChoiceBox.getItems().add(types.get(i));
            this.incomeForLoadTypeChoiceBox.getItems().add(types.get(i));
        }
    }

    private void datePickerOptions() {
        //Expense tab
        this.expCreateDatePicker.setValue(LocalDate.now());
        this.ExpenseDatePickerFrom.setValue(DataHelper.firstMonthDay());
        this.expenseDatePickerTo.setValue(DataHelper.tomorrowDay());

        //Income tab
        this.incCreateDatePicker.setValue(LocalDate.now());
        this.incomeDatePickerFrom.setValue(DataHelper.firstMonthDay());
        this.incomeDatePickerTo.setValue(DataHelper.tomorrowDay());

        // Expense pie chart tab
        expensesPieChartDateFrom.setValue(DataHelper.firstMonthDay());
        expensesPieChartDateTo.setValue(DataHelper.tomorrowDay());

        // Income pie chart tab
        incomesPieChartDateFrom.setValue(DataHelper.firstMonthDay());
        incomesPieChartDateTo.setValue(DataHelper.tomorrowDay());

        // Balance tab
        balanceAreaChartDateFrom.setValue(DataHelper.lastYearCurrentDay());
        balanceAreaChartDateTo.setValue(DataHelper.tomorrowDay());
    }

    private void choiceBoxOptions() {
        //Expenses tab
        this.expTypeChoiceBox.setValue(" Type Selection");
        this.expenseForLoadTypeChoiceBox.setValue(" Type Selection");

        //Incomes tab
        this.incTypeChoiceBox.setValue(" Type Selection");
        this.incomeForLoadTypeChoiceBox.setValue(" Type Selection");
    }
}
