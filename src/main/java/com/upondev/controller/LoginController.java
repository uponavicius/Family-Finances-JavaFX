package com.upondev.controller;

import com.upondev.services.Check;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    private static String loginUserName;

    public static String getLoginUserName() {
        return loginUserName;
    }

    @FXML
    private Label label;
    @FXML
    private TextField userName;
    @FXML
    private PasswordField password;
    @FXML
    private Button login;
    @FXML
    private Button singUpButton;
    @FXML
    private Button loginUserOptionsButton;

    @FXML
    public void loginButtonAction(ActionEvent event) {
        login(event);
    }

    @FXML
    private void singUpButtonAction(ActionEvent event) {
        Parent parent = null;
        Stage stage = new Stage();
        try {
            parent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/singUp.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setTitle("Family Finances - create new user");
        stage.show();
    }

    @FXML
    private void loginUserOptionsButtonAction(ActionEvent event) throws IOException {
        Parent parent = null;
        Stage stage = new Stage();
        parent = FXMLLoader.load(getClass().getResource("/fxml/userOptions.fxml"));
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setTitle("Family Finances - Login");
        stage.show();
    }

    private void login(ActionEvent event) {
        if (Check.findUserForLogin(userName.getText(), password.getText()) == true) {
            label.setText("Login success");
            this.loginUserName = userName.getText();
            Parent userDateViewParent = null;
            try {
                userDateViewParent = (Parent) FXMLLoader.load(getClass().getResource("/fxml/userData.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Scene userDateViewScene = new Scene(userDateViewParent);
            // windows (scene) switch
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(userDateViewScene);
            stage.show();
            stage.setTitle("Family Finances");

        } else {
            label.setText("Incorrect user name or password");
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // password.setOnKeyPressed(e -> {
        //     if (e.getCode() == KeyCode.ENTER) {
        //         // login(event);
        //     }
        // });
    }


}
