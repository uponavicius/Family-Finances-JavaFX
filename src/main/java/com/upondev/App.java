package com.upondev;

import com.upondev.util.HibernateUtil;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;;
import javafx.stage.Stage;

import java.time.LocalDate;


public class App extends Application {

    public static void main(String[] args) {

        try {
            HibernateUtil.getSessionFactory();
            launch();

        } finally {
            HibernateUtil.shutdown();
        }


    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent parent = FXMLLoader.load(getClass().getResource("/fxml/login.fxml"));
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setTitle("Family Finances - Login");
        stage.show();
    }

}
