package com.upondev.dao;

import com.upondev.model.Expense;
import com.upondev.model.Income;
import com.upondev.services.DAOHelper;
import com.upondev.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExpenseIncomeDAO {
    public static void createEntry(String userName, String typeName, Date date, String entryName, double entryValue, Class entityClass) {
        if (entityClass == Expense.class) {
            Expense entry = createExpenseObject(userName, typeName, date, entryName, entryValue);
            DAOHelper.hibernateSessionSave(entry);
        } else if (entityClass == Income.class) {
            Income entry = createIncomeObject(userName, typeName, date, entryName, entryValue);
            DAOHelper.hibernateSessionSave(entry);
        } else {
            System.out.println("Incorrect entity class");
        }
    }

    private static Income createIncomeObject(String userName, String typeName, Date date, String entryName, double entryValue) {
        Income entry = new Income();
        entry.setUserName(userName);
        entry.setTypeName(typeName);
        entry.setEntryDate(date);
        entry.setEntryName(entryName);
        entry.setEntryValue(entryValue);
        return entry;
    }

    private static Expense createExpenseObject(String userName, String typeName, Date date, String entryName, double entryValue) {
        Expense entry = new Expense();
        entry.setUserName(userName);
        entry.setTypeName(typeName);
        entry.setEntryDate(date);
        entry.setEntryName(entryName);
        entry.setEntryValue(entryValue);
        return entry;
    }

    public static void updateEntry(String userName, String typeName, Date date, String entryName, double entryValue, Class entityClass) {
        if (entityClass == Expense.class) {
            Expense entry = createExpenseObject(userName, typeName, date, entryName, entryValue);
            DAOHelper.hibernateSessionUpdate(entry);
        } else if (entityClass == Income.class) {
            Income entry = createIncomeObject(userName, typeName, date, entryName, entryValue);
            DAOHelper.hibernateSessionUpdate(entry);
        } else {
            System.out.println("Incorrect entity class");
        }
    }

    public static void deleteEntry(String userName, String typeName, Date date, String entryName, double entryValue, Class entityClass) {
        if (entityClass == Expense.class) {
            Expense entry = createExpenseObject(userName, typeName, date, entryName, entryValue);
            DAOHelper.hibernateSessionDelete(entry);
        } else if (entityClass == Income.class) {
            Income entry = createIncomeObject(userName, typeName, date, entryName, entryValue);
            DAOHelper.hibernateSessionDelete(entry);
        } else {
            System.out.println("Incorrect entity class");
        }
    }

    public static List<Object> getEntries(String userName, Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    // .getSession()
                    .createCriteria(entityClass)
                    .add(Restrictions.eq("userName", userName))
                    .addOrder(Order.desc("entryDate"));

            List<Object> entries = criteria.list();
            return entries;
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public static List<Object> getEntriesBetweenTwoDays(String userName, Date dateFrom, Date dateTo, Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    // .getSession()
                    .createCriteria(entityClass)
                    .add(Restrictions.eq("userName", userName))
                    .add(Restrictions.between("entryDate",
                            dateFrom,
                            dateTo))
                    .addOrder(Order.desc("entryDate"));

            List<Object> entries = criteria.list();
            return entries;
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public static List<Object> entriesBetweenTwoDaysAndType(String userName, Date dateFrom, Date dateTo, String typeName, Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    // .getSession()
                    .createCriteria(entityClass)
                    .add(Restrictions.eq("userName", userName))
                    .add(Restrictions.between("entryDate",
                            dateFrom,
                            dateTo))
                    .add(Restrictions.eq("typeName", typeName))
                    .addOrder(Order.desc("entryDate"));

            List<Object> entries = criteria.list();
            return entries;
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public static Map<String, Double> typesValuesSum(String userName, Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        Map<String, Double> typesValuesSum = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    // .getSession()
                    .createCriteria(entityClass)
                    .add(Restrictions.eq("userName", userName))
                    .addOrder(Order.desc("typeName"));

            if (entityClass == Expense.class) {
                List<Expense> entries = criteria.list();
                typesValuesSum = entries.stream().collect(
                        Collectors.toMap(Expense::getTypeName, Expense::getEntryValue,
                                (oldValue, newValue) -> oldValue + newValue));

            } else if (entityClass == Income.class) {
                List<Income> entries = criteria.list();
                typesValuesSum = entries.stream()
                        .collect(
                                Collectors.toMap(Income::getTypeName, Income::getEntryValue,
                                        (oldValue, newValue) -> oldValue + newValue));
            }

        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }

        return typesValuesSum;
    }

    public static Map<String, Double> typesValuesBetweenToDaysSum(String userName, Date dateFrom, Date dateTo, Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        Map<String, Double> typesValuesSum = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    // .getSession()
                    .createCriteria(entityClass)
                    .add(Restrictions.eq("userName", userName))
                    .add(Restrictions.between("entryDate", dateFrom, dateTo))
                    .addOrder(Order.desc("typeName"));

            if (entityClass == Expense.class) {
                List<Expense> entries = criteria.list();
                typesValuesSum = entries.stream()
                        .collect(Collectors.toMap(Expense::getTypeName, Expense::getEntryValue,
                                (oldValue, newValue) -> oldValue + newValue));

            } else if (entityClass == Income.class) {
                List<Income> entries = criteria.list();
                typesValuesSum = entries.stream()
                        .collect(Collectors.toMap(Income::getTypeName, Income::getEntryValue,
                                (oldValue, newValue) -> oldValue + newValue));
            }

        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }

        return typesValuesSum;
    }

}
