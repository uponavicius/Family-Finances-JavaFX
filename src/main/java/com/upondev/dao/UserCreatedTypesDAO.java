package com.upondev.dao;

import com.upondev.model.UserExpenseType;
import com.upondev.model.UserIncomeType;
import com.upondev.services.DAOHelper;
import com.upondev.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class UserCreatedTypesDAO {
    public static void createType(String userName, String typeName, Class entityClass) {
        if (entityClass == UserExpenseType.class) {
            UserExpenseType type = getUserExpenseType(userName, typeName);
            DAOHelper.hibernateSessionSave(type);
        } else if (entityClass == UserIncomeType.class) {
            UserIncomeType type = getUserIncomeType(userName, typeName);
            DAOHelper.hibernateSessionSave(type);
        } else {
            System.out.println("Incorrect entity class");
        }
    }

    public void updateTypeName(String userName, String typeName, Class entityClass) {
        if (entityClass == UserExpenseType.class) {
            UserExpenseType type = getUserExpenseType(userName, typeName);
            DAOHelper.hibernateSessionUpdate(type);
        } else if (entityClass == UserIncomeType.class) {
            UserIncomeType type = getUserIncomeType(userName, typeName);
            DAOHelper.hibernateSessionUpdate(type);
        } else {
            System.out.println("Incorrect entity class");
        }
    }

    public static void deleteTypeEntry(String userName, String typeName, Class entityClass) {
        if (entityClass == UserExpenseType.class) {
            UserExpenseType type = getUserExpenseType(userName, typeName);
            DAOHelper.hibernateSessionDelete(type);
        } else if (entityClass == UserIncomeType.class) {
            UserIncomeType type = getUserIncomeType(userName, typeName);
            DAOHelper.hibernateSessionDelete(type);
        } else {
            System.out.println("Incorrect entity class");
        }
    }

    // //TODO
    // // public static void deleteAllT(String userName, Class entityClass) {
    // //     if (entityClass == UserExpenseType.class) {
    // //         deleteUserEntries(userName, entityClass);
    // //     } else if (entityClass == UserIncomeType.class) {
    // //
    // //
    // //
    // //
    // //
    // //     } else if (){
    // //
    // //     } else if (){
    // //
    // //     } else{}
    // // }
    //
    // //TODO perkelti kitur, nes tinka visoms klasems
    // private static void deleteUserEntries(String userName, Class entityClass) {
    //     Session session = null;
    //     Transaction transaction = null;
    //     try {
    //         session = HibernateUtil.getSessionFactory().openSession();
    //         transaction = session.beginTransaction();
    //         Criteria criteria = session
    //                 .createCriteria(entityClass)
    //                 .add(Restrictions.eq("userName", userName));
    //         session.delete(criteria);
    //         transaction.commit();
    //     } catch (Exception ex) {
    //         DAOHelper.hibernateTransactionRollback(transaction);
    //         ex.printStackTrace();
    //     } finally {
    //         session.close();
    //     }
    // }

    private static UserIncomeType getUserIncomeType(String userName, String typeName) {
        UserIncomeType type = new UserIncomeType();
        type.setUserName(userName);
        type.setTypeName(typeName);
        return type;
    }

    private static UserExpenseType getUserExpenseType(String userName, String typeName) {
        UserExpenseType type = new UserExpenseType();
        type.setUserName(userName);
        type.setTypeName(typeName);
        return type;
    }


    public static Object getUserType(int id, String userName, Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Object object = session
                    .createCriteria(entityClass)
                    .add(Restrictions.eq("id", id))
                    .add(Restrictions.eq("userName", userName)).uniqueResult();
            return object;
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public static List<Object> getUserCreatedTypes(String userName, Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    .createCriteria(entityClass)
                    .add(Restrictions.eq("userName", userName))
                    .addOrder(Order.desc("typeName"));
            List<Object> entries = criteria.list();

            transaction.commit();
            return entries;
        } catch (Exception ex) {

            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }
}
