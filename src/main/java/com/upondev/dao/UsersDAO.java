package com.upondev.dao;

import com.upondev.model.*;
import com.upondev.services.Check;
import com.upondev.services.DAOHelper;
import com.upondev.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class UsersDAO {

    public static void  createUser(String userName, String email, String pass) {
        if (Check.emailRegex(email)) {
            if (Check.passwordRegex(pass)) {
                if (!Check.checkOrUserNameExist(userName)) {
                    if (!Check.checkOrEmailExist(email)) {
                        Users user = new Users();
                        user.setUserName(userName);
                        user.setEmail(email);
                        user.setPass(pass);
                        DAOHelper.hibernateSessionSave(user);
                    } else {
                        System.out.println("Email address " + email + " already registered");
                    }
                } else {
                    System.out.println("User name " + userName + " already registered");
                }
            } else {
                System.out.println("Incorrect password");
            }

        } else {
            System.out.println("Incorrect email address");
        }
    }

    public static void updateUserName(int userId, String userName, Class entityClass) {
        if (Check.checkOrUserNameExist(userName)) {
            System.out.println("User name " + userName + " already exist");
        } else {
            Users fromDB = (Users) getUser(userId);
            Users newUser = new Users();
            newUser.setUserId(userId);
            newUser.setUserName(userName);
            newUser.setEmail(fromDB.getEmail());
            newUser.setPass(fromDB.getPass());
            DAOHelper.hibernateSessionMerge(newUser);
        }

    }

    public static void updateEmail(int userId, String email, Class entityClass) {
        if (Check.emailRegex(email)) {
            if (Check.checkOrEmailExist(email)) {
                System.out.println("Email " + email + " already registered");
            } else {
                Users fromDB = (Users) getUser(userId);
                Users newUser = new Users();
                newUser.setUserId(userId);
                newUser.setUserName(fromDB.getUserName());
                newUser.setEmail(email);
                newUser.setPass(fromDB.getPass());
                DAOHelper.hibernateSessionMerge(newUser);
            }
        } else {
            System.out.println("Incorrect email address");
        }


    }

    public static void updatePassword(int userId, String password, Class entityClass) {
        if (Check.passwordRegex(password)) {
            Users fromDB = (Users) getUser(userId);
            Users newUser = new Users();
            newUser.setUserId(userId);
            newUser.setUserName(fromDB.getUserName());
            newUser.setEmail(fromDB.getEmail());
            newUser.setPass(password);
            DAOHelper.hibernateSessionMerge(newUser);
        } else {
            System.out.println("Incorrect password");
            System.out.println("Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character");
        }


    }

    // public void deleteUser(int userId) { // https://www.codejava.net/frameworks/hibernate/hibernate-basics-3-ways-to-delete-an-entity-from-the-datastore
    //     Session session = null;
    //     Transaction transaction = null;
    //     try {
    //         session = HibernateUtil.getSessionFactory().openSession();
    //         transaction = session.beginTransaction();
    //
    //         if (session.get(Users.class, userId) != null) {
    //             Users user = (Users) session
    //                     .createCriteria(Users.class)
    //                     .add(Restrictions.eq("userId", userId)).uniqueResult();
    //             session.delete(user);
    //             transaction.commit();
    //         }
    //     } catch (Exception ex) {
    //         DAOHelper.hibernateTransactionRollback(transaction);
    //         ex.printStackTrace();
    //     } finally {
    //         session.close();
    //     }
    // }

    public static Users getUser(int id) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Users user = session.get(Users.class, id);
            System.out.println(user);
            return user;
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public static Users getUser(String userName) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Users user = new Users();
            Criteria criteria = session
                    .createCriteria(Users.class)
                    .add(Restrictions.eq("userName", userName))
                    .addOrder(Order.asc("userName"));
            List<Users> users = criteria.list();
            Users userFromDB = users.get(0);
            user.setUserId(userFromDB.getUserId());
            user.setUserName(userFromDB.getUserName());
            user.setEmail(userFromDB.getEmail());
            user.setPass(userFromDB.getPass());
            user.setEntryDate(userFromDB.getEntryDate());

            return user;
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public static void deleteAllUserEntriesFromOneTable(String userName, Class entityClass) {
        if (entityClass == Expense.class) {
            String query = "delete from Expense where userName= :userName";
            deleteUserDateFromDB(query);
        } else if (entityClass == Income.class) {
            String query = "delete from Income where userName= :userName";
            deleteUserDateFromDB(query);
        } else if (entityClass == UserExpenseType.class) {
            String query = "delete from UserExpenseType where userName= :userName";
            deleteUserDateFromDB(query);
        } else if (entityClass == UserIncomeType.class) {
            String query = "delete from UserIncomeType where userName= :userName";
            deleteUserDateFromDB(query);
        } else if (entityClass == Users.class) {
            String query = "delete from Users where userName= :userName";
            deleteUserDateFromDB(query);
        } else {
            System.out.println("Incorrect method parameters!");
        }
    }

    private static void deleteUserDateFromDB(String query) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            String delete = query;
            session.createQuery(delete).setString("userName", "upondev").executeUpdate();

            transaction.commit();
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

}
