package com.upondev.dao;

import com.upondev.model.DefaultExpensesType;
import com.upondev.model.DefaultIncomeType;
import com.upondev.services.DAOHelper;
import com.upondev.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class DefaultTypesDAO {
    //this method using just test user  for create entries to DB.
    public static void create(String typeName, Class entityClass) {
        if (entityClass == DefaultExpensesType.class) {
            DefaultExpensesType defaultExpensesType = new DefaultExpensesType();
            defaultExpensesType.setTypeName(typeName);
            Session session = null;
            Transaction transaction = null;
            try {
                session = HibernateUtil.getSessionFactory().openSession();
                transaction = session.beginTransaction();
                Object object = session
                        .createCriteria(entityClass);

                DAOHelper.hibernateSessionSave(typeName);
            } catch (Exception ex) {
                DAOHelper.hibernateTransactionRollback(transaction);
                ex.printStackTrace();
            } finally {
                session.close();
            }
        } else if (entityClass == DefaultIncomeType.class) {
            DefaultIncomeType defaultIncomeType = new DefaultIncomeType();
            defaultIncomeType.setTypeName(typeName);
            Session session = null;
            Transaction transaction = null;
            try {
                session = HibernateUtil.getSessionFactory().openSession();
                transaction = session.beginTransaction();
                Object object = session
                        .createCriteria(entityClass);

                DAOHelper.hibernateSessionSave(typeName);
            } catch (Exception ex) {
                DAOHelper.hibernateTransactionRollback(transaction);
                ex.printStackTrace();
            } finally {
                session.close();
            }
        }
    }

    public static Object getDefaultType(int typeId, Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Object object = session
                    .createCriteria(entityClass)
                    .add(Restrictions.eq("entryId", typeId)).uniqueResult();
            return object;
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }

    public static List<Object> getDefaultTypes(Class entityClass) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    .createCriteria(entityClass)
                    .addOrder(Order.desc("typeName"));
            List<Object> entries = criteria.list();

            transaction.commit();
            return entries;
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }


}
