package com.upondev.services;

import javafx.scene.control.DatePicker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.*;

public class DataHelper {

    public static Date getFormattedFromDateTime(String dateFrom) {
        Date date = null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(dateFrom);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        // System.out.println(cal.getTime());
        return cal.getTime();
    }

    public static Date getFormattedToDateTime(String dateTo) {
        Date date = null;

        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(dateTo);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        // System.out.println(cal.getTime());
        return cal.getTime();
    }

    public static Date stringToDateForView(String date) {
        DateFormat formatter;
        Date d = null;
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            d = formatter.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    public static Date localDateToDate(DatePicker datePicker) {
        ZoneId defaultZoneId = ZoneId.systemDefault();
        LocalDate localDate = datePicker.getValue();
        return Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
    }

    public static Date localDateToDate(LocalDate localDate) {
        Date date = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        return date;
    }

    public static String localDateToDateToString(DatePicker datePicker) {
        ZoneId defaultZoneId = ZoneId.systemDefault();
        LocalDate localDate = datePicker.getValue();
        LocalDateTime localDateTime = localDate.atStartOfDay();
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
        String[] arr = localDateTime.format(formatter).split("T");
        String date = arr[0];
        return date;
    }

    public static LocalDate firstMonthDay() {
        LocalDate date = LocalDate.now();
        LocalDate firstDayOfMonth = date.with(TemporalAdjusters.firstDayOfMonth());
        return firstDayOfMonth;
    }

    public static LocalDate tomorrowDay() {
        LocalDate today = LocalDate.now().plusDays(1);
        return today;
    }

    public static LocalDate lastYearCurrentDay() {
        LocalDate today = LocalDate.now().minusYears(1);
        return today;
    }

}
