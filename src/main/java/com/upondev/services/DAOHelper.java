package com.upondev.services;

import com.upondev.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DAOHelper {
           /*
    Session session = HibernateUtil.getSessionFactory().openSession();
    Session session = HibernateUtil.getSessionFactory().getCurrentSession();

    if you use sessionFactory.getCurrentSession(), you'll obtain a "current session" which is bound to the lifecycle of
    the transaction and will be automatically flushed and closed when the transaction ends (commit or rollback).

    if you decide to use sessionFactory.openSession(), you'll have to manage the session yourself and to flush and close it "manually".
     */

    public static void hibernateTransactionRollback(Transaction transaction) {
        if (transaction != null) {
            transaction.rollback();
        }
    }

    public static void hibernateSessionSave(Object object) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(object);
            transaction.commit();
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    public static void hibernateSessionUpdate(Object object) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(object);
            transaction.commit();
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    public static void hibernateSessionDelete(Object object) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(object);
            transaction.commit();
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }

    public static void hibernateSessionMerge(Object object) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(object);
            transaction.commit();
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        } finally {
            session.close();
        }
    }


}
