package com.upondev.services;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapHelper {

    // public static Map<Integer, String> expListToMap(List<ExpenseType> list) {
    //     return list
    //             .stream()
    //             .collect(Collectors.toMap(ExpenseType::getEntryId, ExpenseType::getEntryName));
    // }
    //
    // public static Map<Integer, String> incListToMap(List<IncomeType> list) {
    //     return list
    //             .stream()
    //             .collect(Collectors.toMap(IncomeType::getEntryId, IncomeType::getEntryName));
    // }

    public static Map<Integer, String> sortIntegerString(Map<Integer, String> unsorted) {
        return unsorted.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }



    public static void print(Map<String, Double> map) {
        for (Map.Entry<String, Double> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " " + NumberHelper.roundTwoDem(entry.getValue()));
        }
    }

    public static Map<Integer, Double> sortIntegerDouble(Map<Integer, Double> unsorted) {
        return unsorted.entrySet()
                .stream()
                .sorted((Map.Entry.<Integer, Double>comparingByValue()
                        .reversed()))
                .collect(Collectors.toMap(
                        Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public static  <K, V> K getKey(Map<K, V> map, V value) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            if (entry.getValue().equals(value)) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static void getKey(Map<Integer, String> defaultAndUserTypes) {
    }
}
