package com.upondev.services;

import java.text.DecimalFormat;

public class NumberHelper {

    public static double roundTwoDem(double value) {
        DecimalFormat df = new DecimalFormat("####0.00");
        return Double.parseDouble(df.format(value));
    }

}
