package com.upondev.services;

import com.upondev.model.Expense;
import com.upondev.model.Income;

import java.util.List;

public class Calculation {

    public static double sumExpensesValues(List<Expense> data) {
        Double sum = data
                .stream()
                .mapToDouble(Expense::getEntryValue)
                .sum();
        return sum;
    }

    public static double sumIncomesValues(List<Income> data) {
        Double sum = data
                .stream()
                .mapToDouble(Income::getEntryValue)
                .sum();
        return sum;
    }

}
