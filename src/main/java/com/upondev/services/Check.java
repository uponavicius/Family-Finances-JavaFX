package com.upondev.services;

import com.upondev.model.Users;
import com.upondev.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Check {

    public static boolean checkOrUserHaveEntry(int userId, int entryId, Class EntityClassName) {
        Transaction transaction = null;
        List<Object> obj = new LinkedList<>();
        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.getCurrentSession();
            transaction = session.beginTransaction();

            Criteria criteria = session.createCriteria(EntityClassName)
                    .add(Restrictions.eq("userId", userId))
                    .add(Restrictions.eq("entryId", entryId));
            obj = criteria.list();
            transaction.commit();

        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        }

        if (obj.size() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean checkOrUserNameExist(String userName) {
        Transaction transaction = null;
        List<String> userN = new LinkedList<>();

        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.getCurrentSession();
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Users.class)
                    .add(Restrictions.eq("userName", userName));
            userN = criteria.list();

        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        }
        if (userN.size() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean checkOrEmailExist(String email) {
        Transaction transaction = null;
        List<String> emailAddress = new LinkedList<>();

        try {
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.getCurrentSession();
            transaction = session.beginTransaction();
            Criteria criteria = session.createCriteria(Users.class)
                    .add(Restrictions.eq("email", email));

            emailAddress = criteria.list();
        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        }
        if (emailAddress.size() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean findUserForLogin(String userName, String password) {
        Session session = null;
        List<Users> users = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    .createCriteria(Users.class)
                    .add(Restrictions.eq("userName", userName))
                    .add(Restrictions.eq("pass", password));
            users = criteria.list();

        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        } finally {
            session.close();
        }

        if (users.size() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean findUserForResetPassword(String userName, String email) {
        Session session = null;
        List<Users> users = null;
        Transaction transaction = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            Criteria criteria = session
                    .createCriteria(Users.class)
                    .add(Restrictions.eq("userName", userName))
                    .add(Restrictions.eq("email", email));
            users = criteria.list();

        } catch (Exception ex) {
            DAOHelper.hibernateTransactionRollback(transaction);
            ex.printStackTrace();
        } finally {
            session.close();
        }

        if (users.size() == 1) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean emailRegex(String email) {
        String regex = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(email);
        return matcher.find();
    }

    public static boolean passwordRegex(String pass) {
        /*
        Minimum eight characters, at least one letter and one number:
        "^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"

        Minimum eight characters, at least one letter, one number and one special character:
        "^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$"

        Minimum eight characters, at least one uppercase letter, one lowercase letter and one number:
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$"

        Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character:
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$"

        Minimum eight and maximum 10 characters, at least one uppercase letter, one lowercase letter, one number and one special character:
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$"
         */
        String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(pass);
        return matcher.find();

    }
}