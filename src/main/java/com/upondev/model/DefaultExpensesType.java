package com.upondev.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "default_expenses_types", schema = "FamiFin")
public class DefaultExpensesType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "type_name")
    private String typeName;

    public DefaultExpensesType() {
    }

    public DefaultExpensesType(String typeName) {
        this.typeName = typeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultExpensesType that = (DefaultExpensesType) o;
        return id == that.id &&
                Objects.equals(typeName, that.typeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, typeName);
    }


    @Override
    public String toString() {
        return "DefaultExpensesType{" +
                "id=" + id +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
