package com.upondev.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "incomes", schema = "FamiFin")
public class Income {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int userId;

    @Column(name = "entry_date")
    private Date entryDate;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "type_name")
    private String typeName;

    @Column(name = "entry_name")
    private String entryName;

    @Column(name = "entry_value")
    private double entryValue;

    public Income() {
    }

    public Income(String userName, String typeName, String entryName, double entryValue) {
        this.userName = userName;
        this.typeName = typeName;
        this.entryName = entryName;
        this.entryValue = entryValue;
    }

    public Income(Date entryDate, String userName, String typeName, String entryName, double entryValue) {
        this.entryDate = entryDate;
        this.userName = userName;
        this.typeName = typeName;
        this.entryName = entryName;
        this.entryValue = entryValue;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getEntryName() {
        return entryName;
    }

    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    public double getEntryValue() {
        return entryValue;
    }

    public void setEntryValue(double entryValue) {
        this.entryValue = entryValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Income income = (Income) o;
        return userId == income.userId &&
                Double.compare(income.entryValue, entryValue) == 0 &&
                Objects.equals(entryDate, income.entryDate) &&
                Objects.equals(userName, income.userName) &&
                Objects.equals(typeName, income.typeName) &&
                Objects.equals(entryName, income.entryName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, entryDate, userName, typeName, entryName, entryValue);
    }

    @Override
    public String toString() {
        return "Income{" +
                "userId=" + userId +
                ", entryDate=" + entryDate +
                ", userName='" + userName + '\'' +
                ", typeName='" + typeName + '\'' +
                ", entryName='" + entryName + '\'' +
                ", entryValue=" + entryValue +
                '}';
    }
}
