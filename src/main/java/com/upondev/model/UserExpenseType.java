package com.upondev.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_expenses_types", schema = "FamiFin")
public class UserExpenseType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "type_name")
    private String typeName;

    public UserExpenseType() {
    }

    public UserExpenseType(String userName, String typeName) {
        this.userName = userName;
        this.typeName = typeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserExpenseType that = (UserExpenseType) o;
        return id == that.id &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(typeName, that.typeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, typeName);
    }

    @Override
    public String toString() {
        return "UserExpenseType{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
