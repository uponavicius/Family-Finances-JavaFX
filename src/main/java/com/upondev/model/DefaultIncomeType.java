package com.upondev.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "default_incomes_types", schema = "FamiFin")
public class DefaultIncomeType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "type_name")
    private String typeName;

    public DefaultIncomeType() {
    }

    public DefaultIncomeType(String typeName) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultIncomeType that = (DefaultIncomeType) o;
        return id == that.id &&
                Objects.equals(typeName, that.typeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, typeName);
    }

    @Override
    public String toString() {
        return "DefaultIncomeTypes{" +
                "id=" + id +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
