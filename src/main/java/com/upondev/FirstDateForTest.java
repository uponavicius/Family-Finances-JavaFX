package com.upondev;

import com.upondev.dao.DefaultTypesDAO;
import com.upondev.dao.ExpenseIncomeDAO;
import com.upondev.dao.UserCreatedTypesDAO;
import com.upondev.dao.UsersDAO;
import com.upondev.model.*;
import com.upondev.services.DataHelper;
import org.hibernate.id.uuid.Helper;

import java.time.LocalDate;

public class FirstDateForTest {

    public static void createFirstDate() {

        UsersDAO.createUser("test", "test@test.com", "Test1234?");

        DefaultTypesDAO.create(" Type Selection", DefaultExpensesType.class);
        DefaultTypesDAO.create("Car maintenance", DefaultExpensesType.class);
        DefaultTypesDAO.create("Children", DefaultExpensesType.class);
        DefaultTypesDAO.create("Clothing and footwear", DefaultExpensesType.class);
        DefaultTypesDAO.create("Communications", DefaultExpensesType.class);
        DefaultTypesDAO.create("Credit", DefaultExpensesType.class);
        DefaultTypesDAO.create("Food", DefaultExpensesType.class);
        DefaultTypesDAO.create("Fuel", DefaultExpensesType.class);
        DefaultTypesDAO.create("Gifts and guests", DefaultExpensesType.class);
        DefaultTypesDAO.create("Gifts and guests", DefaultExpensesType.class);
        DefaultTypesDAO.create("Home", DefaultExpensesType.class);
        DefaultTypesDAO.create("Husband personal expenses", DefaultExpensesType.class);
        DefaultTypesDAO.create("Kindergarten", DefaultExpensesType.class);
        DefaultTypesDAO.create("Other expenses", DefaultExpensesType.class);
        DefaultTypesDAO.create("Entertainments", DefaultExpensesType.class);
        DefaultTypesDAO.create("Sport", DefaultExpensesType.class);

        DefaultTypesDAO.create(" Type Selection", DefaultIncomeType.class);
        DefaultTypesDAO.create("Husband incomes", DefaultIncomeType.class);
        DefaultTypesDAO.create("Wife incomes", DefaultIncomeType.class);
        DefaultTypesDAO.create("Other incomes", DefaultIncomeType.class);

        UserCreatedTypesDAO.createType("Test", "Business building expenses", UserExpenseType.class);
        UserCreatedTypesDAO.createType("Test", "Help for parents", UserExpenseType.class);
        UserCreatedTypesDAO.createType("Test", "Husband business expenses", UserExpenseType.class);
        UserCreatedTypesDAO.createType("Test", "Rent", UserExpenseType.class);
        UserCreatedTypesDAO.createType("Test", "Wife business expenses", UserExpenseType.class);

        UserCreatedTypesDAO.createType("Test", "Husband business incomes", UserIncomeType.class);
        UserCreatedTypesDAO.createType("Test", "Husband other incomes", UserIncomeType.class);
        UserCreatedTypesDAO.createType("Test", "Wife business incomes", UserIncomeType.class);
        UserCreatedTypesDAO.createType("Test", "Wife Other incomes", UserIncomeType.class);

        ExpenseIncomeDAO.createEntry("Test", "Food", DataHelper.localDateToDate(LocalDate.now()), "Pizaa", 15.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Food", DataHelper.localDateToDate(LocalDate.now()), "Kebab", 4.5, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Food", DataHelper.localDateToDate(LocalDate.now()), "Fruits", 15.77, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Food", DataHelper.localDateToDate(LocalDate.now()), "Vegetables", 15.58, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Food", DataHelper.localDateToDate(LocalDate.now()), "Fruit", 13.76, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Communications", DataHelper.localDateToDate(LocalDate.now()), "Internet", 11.33, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Credit", DataHelper.localDateToDate(LocalDate.now()), "Condo", 300.00, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Fuel", DataHelper.localDateToDate(LocalDate.now()), "Gasoline", 56.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Fuel", DataHelper.localDateToDate(LocalDate.now()), "Gasoline", 28.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Gifts and guests", DataHelper.localDateToDate(LocalDate.now()), "Gift for mother", 179.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Health and medicines", DataHelper.localDateToDate(LocalDate.now()), "Vitamins", 179.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Home", DataHelper.localDateToDate(LocalDate.now()), "New bed", 179.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Husband personal expenses", DataHelper.localDateToDate(LocalDate.now()), "New phone", 179.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Kindergarten", DataHelper.localDateToDate(LocalDate.now()), "Kindergarten", 75.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Entertainments", DataHelper.localDateToDate(LocalDate.now()), "A trip by the sea", 275.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Sport", DataHelper.localDateToDate(LocalDate.now()), "Sports club", 275.99, Expense.class);

        ExpenseIncomeDAO.createEntry("Test", "Kindergarten", DataHelper.localDateToDate(LocalDate.now().minusMonths(1)), "Kindergarten", 75.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Entertainments", DataHelper.localDateToDate(LocalDate.now().minusMonths(1)), "A trip by the sea", 275.99, Expense.class);
        ExpenseIncomeDAO.createEntry("Test", "Sport", DataHelper.localDateToDate(LocalDate.now().minusMonths(1)), "Sport club", 59.99, Expense.class);

        ExpenseIncomeDAO.createEntry("Test", "Husband incomes", DataHelper.localDateToDate(LocalDate.now()), "Salary", 2400.00, Income.class);
        ExpenseIncomeDAO.createEntry("Test", "Wife incomes", DataHelper.localDateToDate(LocalDate.now()), "Salary", 1200.00, Income.class);
        ExpenseIncomeDAO.createEntry("Test", "Wife Other incomes", DataHelper.localDateToDate(LocalDate.now()), "Lottery", 1200.00, Income.class);
        ExpenseIncomeDAO.createEntry("Test", "Husband incomes", DataHelper.localDateToDate(LocalDate.now()), "Bonus", 1650.00, Income.class);

        ExpenseIncomeDAO.createEntry("Test", "Husband incomes", DataHelper.localDateToDate(LocalDate.now().minusMonths(1)), "Salary", 1600.00, Income.class);
        ExpenseIncomeDAO.createEntry("Test", "Wife incomes", DataHelper.localDateToDate(LocalDate.now().minusMonths(1)), "Salary", 2000.00, Income.class);
        ExpenseIncomeDAO.createEntry("Test", "Wife incomes", DataHelper.localDateToDate(LocalDate.now().minusMonths(1)), "Bonus", 400.00, Income.class);


    }

}
