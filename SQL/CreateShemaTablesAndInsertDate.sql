create schema famfin;
use famfin;
SET SQL_SAFE_UPDATES = 0;

CREATE TABLE users (
    id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    pass VARCHAR(30) NOT NULL, 
    registration_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE TABLE default_expenses_types (
    id INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    type_name VARCHAR(50) NOT NULL
);
 
CREATE TABLE user_expenses_types (
    id INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(50) NOT NULL,
    type_name VARCHAR(50) NOT NULL
);
 
CREATE TABLE default_incomes_types (
   id INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
   type_name VARCHAR(50) NOT NULL
);

CREATE TABLE user_incomes_types (
    id INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(50) NOT NULL,
    type_name VARCHAR(50) NOT NULL
);

CREATE TABLE expenses (
    id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    entry_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    user_name VARCHAR(50) NOT NULL,
    type_name VARCHAR(50) NOT NULL,
    entry_name VARCHAR(100) NOT NULL,
    entry_value DOUBLE NOT NULL
);
                
CREATE TABLE incomes (
	id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    entry_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
    user_name VARCHAR(50) NOT NULL,
    type_name VARCHAR(50) NOT NULL,
    entry_name VARCHAR(100) NOT NULL,
    entry_value DOUBLE NOT NULL
);                       

INSERT INTO users (id, user_name, email, pass) VALUES ('1','Test', 'test@test.com', 'Test123?');

INSERT INTO default_expenses_types (id, type_name) VALUES
 ('1','Car maintenance'),
 ('2','Children'),
 ('3','Clothing and footwear'),
 ('4','Communications'),
 ('5','Credit'),
 ('6','Food'),
 ('7','Fuel'),
 ('8','Gifts and guests'),
 ('9','Health and medicines'),
 ('10','Home'),
 ('11','Husband personal expenses'),
 ('12','Kindergarten'),
 ('13','Other expenses'),
 ('14','Other products'),
 ('15','Entertainments'),
 ('16','Sport'),
 ('17', ' Type Selection');

INSERT INTO default_incomes_types (id, type_name) VALUES 
('1','Husband incomes'),
('2','Wife incomes'),
('3','Other incomes'),
 ('4', ' Type Selection');

INSERT INTO expenses (user_name, type_name, entry_name, entry_value) VALUES
('Test', 'Food', 'Pizaa', '15.99'), 
('Test', 'Food', 'Kebab', '4.5'), 
('Test', 'Food', 'Fruits', '14.78'),
('Test', 'Food', 'Vegetables', '15.58'), 
('Test', 'Food', 'Fruits', '14.78'),
('Test', 'Communications', 'Inter', '11.15'),
('Test', 'Credit', 'Condo', '250.5'),
('Test', 'Fuel', 'Gasoline', '56.25'),
('Test', 'Fuel', 'Gasoline', '25.58'),
('Test', 'Gifts and guests', 'Gift for mother', '175.68'),
('Test', 'Health and medicines', 'Vitamins', '84.8'),
('Test', 'Home', 'New bed', '458.5'),
('Test', 'Husband personal expenses', 'New phone', '789.99'),
('Test', 'Kindergarten', 'Kindergarten', '75.75'),
('Test', 'Entertainments', 'A trip by the sea ', '258.5'),
('Test', 'Sport', 'Sports club', '58.5');

INSERT INTO expenses (entry_date, user_name, type_name, entry_name, entry_value) VALUES
('2020-01-26', 'Test', 'Credit', 'Car', '700'), 
('2020-01-26', 'Test', 'Food', 'Kebab', '4.5'), 
('2020-01-26','Test', 'Food', 'Fruits', '14.78');

INSERT INTO incomes (user_name, type_name, entry_name, entry_value) VALUES
('Test', 'Husband incomes', 'Salary', '2400'),
('Test', 'Wife incomes', 'Salary', '2400'),
('Test', 'Wife Other incomes', 'Lottery', '1200'),
('Test', 'Husband incomes', 'Bonus', '1650');

INSERT INTO incomes (entry_date, user_name, type_name, entry_name, entry_value) VALUES
('2020-01-26', 'Test', 'Husband incomes', 'Salary', '1600'),
('2020-01-26', 'Test', 'Wife incomes', 'Salary', '2000'),
('2020-01-26', 'Test', 'Husband incomes', 'Bonus', '80');
