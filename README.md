# Family-Finances-JavaFX

This project is Maven project and created with Java 8.

**Run Family-Finances-JavaFX project**


1.  Download or clone Family-Finances-JavaFX project.
2.  If dowloaded Family-Finances-JavaFX-master.zip file, then unzip and open like maven project with Your IDEA.
  If clone project, open like maven project with Your IDEA.
3.  Open MySQL WorkBench and open SQL query "CreateSchemaTablesAndInsertDate.sql" from folder SQL and run.
4.  Run SQL Query "CreateShemaTablesAndInsertDate.sql". 
  - Go to "Family-Finances-JavaFX-master\src\main\resources" and rename file "hibernate.cfg.xml.example" to "hibernate.cfg.xml".
5.  Open hibernate.cfg.xml with text edit (notepad, notepad++ etc.). Change 7th, 8th and 9th rows:
  - 6th row - change "[0.0.0.0]" to IP address or localhost. [DB_NAME] change to famfin.
  - 7th row - [USER_NAME] change to your database user name. 
  - 8th row - [PASSWORD] change to your database password.
6.  In your IDEA go to "Family-Finances-JavaFX-master\src\main\java\com\upondev" open App class and run.
7.  Login with user name - test, password - Test123?




